import React, { createContext, useContext, useState } from 'react'
import Card from 'react-bootstrap/Card';
import { dataContext } from '../../App';
import DisplayCart from '../DisplayCart/DisplayCart';

export const displayCartItems = createContext()
export default function DisplayProduct() {
    const [sendingData, count, handleProduct, handleDecreaseProduct, handleIncreaseProduct, handleRemove, quantity, display, cartItems, handleClose] = useContext(dataContext)

    let totalAmount = 0
    let installment = 0

    if (Array.isArray(cartItems)) {
        cartItems.map((item) => {
            totalAmount = totalAmount + (item.price * quantity[item.id])
            installment = Math.max(installment, (item.installments))
        })
    }

    const checkoutAlert = () => {
        alert(`Checkout - Subtotal: $ ${totalAmount.toFixed(2)}`)
    }
    const allCard = (sendingData.products).map((product) => {
        return (
            <Card key={product.id} className='m-4 mt-0 ms-0 d-flex flex-column align-items-center' style={{ width: '16rem', height: "36rem" }}>
                <Card.Img className='mb-3' variant="top" style={{ height: "62%" }} src={`./products/${product.id}.jpg`} />
                {product.title}
                {product.isFreeShipping &&
                    <div className='d-flex justify-content-end' style={{ width: "100%", position: "absolute" }} >
                        <div className='d-flex justify-content-center' style={{ width: "100px", height: "30px", backgroundColor: "black", color: "white" }}>
                            <p>Free shipping</p>
                        </div>
                    </div>
                }
                <Card.Text className='' style={{ width: "100%", height: "25%" }} >
                    <h3 className=" mt-4 d-flex justify-content-center" >{product.currencyFormat} {product.price}</h3>
                    {product.installments !== 0 ?
                        <h5 className="d-flex justify-content-center" style={{ color: "#9C9B9B" }} >or {product.installments} x {product.currencyFormat} {(product.price / product.installments).toFixed(2)} </h5> : ""}
                </Card.Text>
                <button
                    style={{ width: "100%" }}
                    onClick={() => handleProduct(product)}
                    className="btn btn-dark"
                    type="button"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasScrolling"
                    aria-controls="offcanvasScrolling"  >
                    Add to cart
                </button>
                <div>
                    <displayCartItems.Provider value={
                        [handleClose, display, count, cartItems, quantity, handleDecreaseProduct, handleIncreaseProduct, handleRemove, totalAmount, installment, checkoutAlert]
                    }>
                        <DisplayCart />
                    </displayCartItems.Provider>
                </div>
            </Card>
        )
    })

    return (
        <div className='d-flex flex-column m-5' >
            <p>{allCard.length} Product(s) found</p>
            <div className='d-flex flex-wrap' style={{ width: '70rem', height: "100%" }} >
                {allCard}
            </div>
        </div>
    )
}