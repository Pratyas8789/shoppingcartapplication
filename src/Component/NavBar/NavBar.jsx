import React from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';

export default function NavBar(props) {
    const { count } = props
    return (
        <Navbar className='d-flex justify-content-between p-0' bg="light" variant="dark" >
            <img
                alt=""
                src={`./products/logo.png`}
                width="60"
                height="60"
                style={{ backgroundColor: "black" }}
                className="d-inline-block align-top"
            />{' '}
            <div className='d-flex flex-column' data-bs-toggle="offcanvas" style={{ cursor: "pointer" }}
                data-bs-target="#offcanvasScrolling">
                <img
                    alt=""
                    src={`./products/bag-icon.jpg`}
                    width="50"
                    height="50"
                    style={{ backgroundColor: "black" }}
                    className="d-inline-block align-top"
                />{' '}
                <Button className='ms-3' style={{ borderRadius: "50px" }} variant="warning">{count}</Button>{' '}
            </div>
        </Navbar>
    );
}