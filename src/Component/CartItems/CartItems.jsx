import React, { useContext } from 'react'
import Card from 'react-bootstrap/Card';
import { cartOfItems } from '../DisplayCart/DisplayCart';

export default function CartItems() {
    const [cartItems, quantity, handleDecreaseProduct, handleIncreaseProduct, handleRemove] = useContext(cartOfItems)
    let displayCartItems

    if (Array.isArray(cartItems)) {
        if (cartItems.length > 0) {
            displayCartItems = cartItems.map((item) => {
                return (
                    <div className=' d-flex flex-column align-items-end' key={item.id}   >

                        <Card className='d-flex flex-row mt-4' style={{ width: '100%', height: "150px", backgroundColor: "#1B1A1F", color: "white" }}>
                            <Card.Img style={{ width: "30%", height: "100%" }} src={`./products/${item.id}.jpg`} />
                            <Card.Body className='p-1' style={{ width: "70%", height: "100%" }} >
                                <div className='d-flex justify-content-end' style={{ width: "100%" }} >
                                    <button style={{ backgroundColor: "#1B1A1F", color: "white", height: "20%" }} onClick={() => handleRemove(item.id)} >X</button>
                                </div>
                                <Card.Text className='d-flex justify-content-between m-0' style={{ height: "30%" }}>
                                    <p style={{ width: "70%" }}>{item.title}</p>
                                    <p style={{ width: "30%", color: "#EABF02" }}>{item.currencyFormat} {(item.price * quantity[item.id]).toFixed(2)}</p>
                                </Card.Text>
                                <Card.Text className=' m-0' style={{ height: "25%" }}>
                                    <p>{item.availableSizes[0]} | {item.style}</p>
                                </Card.Text>
                                <Card.Text className='d-flex justify-content-between m-0' style={{ height: "25%" }} >
                                    <p>Qantity: {quantity[item.id]}</p>
                                    <div className='d-flex' style={{ width: "50px" }}>
                                        {quantity[item.id] > 1 ?
                                            <button style={{ width: "50%" }} onClick={() => handleDecreaseProduct(item.id)}>-</button> : <button style={{ width: "50%" }} disabled onClick={() => handleDecreaseProduct(item.id)}>-</button>}
                                        <button style={{ width: "50%" }} onClick={() => handleIncreaseProduct(item.id)} >+</button>
                                    </div>
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                );
            })
        }
    }
    return (
        <div>
            {displayCartItems}
        </div>
    )
}
