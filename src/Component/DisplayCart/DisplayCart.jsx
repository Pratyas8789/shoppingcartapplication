import React, { createContext, useContext } from 'react'
import Button from 'react-bootstrap/Button';
import CartItems from '../CartItems/CartItems';
import Card from 'react-bootstrap/Card';
import { displayCartItems } from '../DisplayProduct/DisplayProduct';

export const cartOfItems = createContext()
export default function DisplayCart() {
    const [handleClose, display, count, cartItems, quantity, handleDecreaseProduct, handleIncreaseProduct, handleRemove, totalAmount, installment, checkoutAlert] = useContext(displayCartItems)

    return (
        <div
            className={`offcanvas offcanvas-end ${display}`}
            data-bs-scroll="true"
            data-bs-backdrop="false"
            tabIndex={-1}
            id="offcanvasScrolling"
            aria-labelledby="offcanvasScrollingLabel"
            style={{ width: '28%' }}
        >
            <div className="offcanvas-header" style={{ backgroundColor: "#1B1A1F", color: "white" }}  >
                <h5 className="offcanvas-title" id="offcanvasScrollingLabel">
                </h5>
                <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="offcanvas"
                    aria-label="Close"
                    onClick={handleClose}
                    style={{ backgroundColor: 'lightgray' }}
                />
            </div>
            <div className='d-flex justify-content-center ' style={{ width: "100%", height: "100px", backgroundColor: "#1B1A1F" }} >
                <div style={{ width: "30%", height: "90%" }} >
                    <div className='d-flex justify-content-between align-items-center' >
                        <img
                            alt=""
                            src={`./products/bag-icon.jpg`}
                            width="50"
                            height="50"
                            style={{ backgroundColor: "#1B1A1F" }}
                        />{' '}
                        <h5 style={{ color: "white" }} >Cart</h5>
                    </div>
                    <Button className='ms-3' style={{ borderRadius: "50px" }} variant="warning">{count}</Button>{' '}
                </div>
            </div>
            <div className="offcanvas-body" style={{ backgroundColor: "#1B1A1F" }}>


                <cartOfItems.Provider value={
                    [cartItems, quantity, handleDecreaseProduct, handleIncreaseProduct, handleRemove]
                } >
                    <CartItems />

                </cartOfItems.Provider>


            </div>
            <div style={{ width: "100%", height: "200px", backgroundColor: "#1B1A1F" }} >
                <Card style={{ width: '100%', backgroundColor: "#1B1A1F", color: "white" }}>
                    <Card.Body >
                        <Card.Text className='d-flex justify-content-between'  >
                            <h4 >SUBTOTAL</h4>
                            <div className='d-flex flex-column align-items-end'   >
                                <h4 style={{ color: "#EABF02" }} >${totalAmount.toFixed(2)}</h4>
                                <p>OR UP TO {installment}x {(totalAmount / installment).toFixed(2)} </p>
                            </div>
                        </Card.Text>
                        <Button onClick={checkoutAlert} style={{ width: "100%" }} variant="dark">CECKOUT</Button>
                    </Card.Body>
                </Card>
            </div>
        </div>
    )
}
