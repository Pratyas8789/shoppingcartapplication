import React, { useContext } from 'react'
import Card from 'react-bootstrap/Card';
import { sizeContext } from '../../App';

export default function AvailableSizes() {
  const allSize=useContext(sizeContext)
  return (
    <Card className='m-5' style={{ width: '18rem', height: "fit-content"}}>
      <Card.Body  >
        <Card.Title>Sizes:</Card.Title>
        <Card.Text className='d-flex flex-wrap' >
          {allSize}
        </Card.Text>
      </Card.Body>
    </Card>
  )
}