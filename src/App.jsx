import 'bootstrap/dist/css/bootstrap.min.css';
import AvailableSizes from './Component/AvailableSizes/AvailableSizes';
import React, { createContext, useRef, useState } from 'react'
import './App.css'
import DisplayProduct from './Component/DisplayProduct/DisplayProduct';
import data from './Component/Data/shopping-cart/data.json'
import NavBar from './Component/NavBar/NavBar';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js'

export const sizeContext = createContext()
export const dataContext = createContext()

function App() {
  const [itemSize, setItemSize] = useState([])
  const [count, setCount] = useState(0)
  const [quantity, setQuantity] = useState({})
  const [isOpen, setIsOpen] = useState(false)
  let display = isOpen ? 'show' : ""
  const [cartItems, setCartItems] = useState([])

  const handleProduct = (product) => {
    setIsOpen(true)
    if (quantity.hasOwnProperty(product.id)) {
      const productId = product.id
      const value = quantity[productId] + 1
      setQuantity(existingQuantity => ({
        ...existingQuantity,
        [productId]: value,
      }))
      setCount((preCount) => preCount + 1)
    } else {
      const productId = product.id
      const value = 1
      setQuantity(existingQuantity => ({
        ...existingQuantity,
        [productId]: value,
      }))
      const copyOfCartItems = [...cartItems]
      copyOfCartItems.push(product)
      setCartItems(copyOfCartItems)
      setCount((preCount) => preCount + 1)
    }
  }

  const handleDecreaseProduct = (id) => {
    const value = quantity[id] - 1
    setQuantity(existingQuantity => ({
      ...existingQuantity,
      [id]: value,
    }))
    setCount((preCount) => preCount - 1)
  }
  const handleIncreaseProduct = (id) => {
    const value = quantity[id] + 1
    setQuantity(existingQuantity => ({
      ...existingQuantity,
      [id]: value,
    }))
    setCount((preCount) => preCount + 1)
  }

  const handleClose = () => {
    setIsOpen(false)
  }

  const handleRemove = (id) => {
    const value = quantity[id]
    delete quantity[id];
    setQuantity({ ...quantity });
    setCount((preCount) => preCount - value)
    setCartItems((preitem) => {
      const filteredList = (preitem).filter(item => item.id !== id)
      return filteredList
    })
  }

  const obj = {}
  if (Array.isArray(data.products)) {
    data.products.map((product) => {
      const productSize = product.availableSizes
      productSize.map((size) => {
        obj[size] = false
      })
    })
  }

  const [theme, settheme] = useState(obj)
  const handleData = (size) => {
    const copyOfSize = [...itemSize]
    if (copyOfSize.includes(size)) {
      const index = copyOfSize.indexOf(size);
      copyOfSize.splice(index, 1)

      settheme(pretheme => ({
        ...pretheme,
        [size]: false
      }))
    } else {
      copyOfSize.push(size)
      settheme(pretheme => ({
        ...pretheme,
        [size]: true
      }))
    }
    setItemSize(copyOfSize)
  }

  let filteredData = {}
  const updatedData = {}
  if (Array.isArray(itemSize)) {
    if (itemSize.length > 0) {
      itemSize.map((size) => {
        const sizeItem = (data.products).filter(item => (item.availableSizes).includes(size))
        for (let product of sizeItem) {
          updatedData[product.id] = product
        }
      })
    }
  }
  if (Object.keys(updatedData).length > 0) {
    filteredData["products"] = Object.values(updatedData)
  }
  const allSize = (Object.entries(obj)).map((size) => {
    return (
      <button key={size} id={`button-${size[0]}`} onClick={() => handleData(size[0])} className={`m-2 `} style={{ width: "50px", height: "50px", borderRadius: "50%", backgroundColor: `${theme[size[0]] ? 'black' : 'white'}`, color: `${theme[size[0]] ? 'white' : 'black'}` }} >
        {size[0]}
      </button>
    )
  })
  const sendingData = Object.keys(filteredData).length > 0 ? filteredData : data
  return (
    <>
      <sizeContext.Provider value={allSize} >
        <dataContext.Provider value={
          [
            sendingData,
            count, handleProduct, handleDecreaseProduct, handleIncreaseProduct, handleRemove, quantity, display, cartItems, handleClose
          ]
        } >
          <NavBar count={count} />
          <div className="App d-flex" style={{ width: "100%", height: "100%" }} >
            <AvailableSizes />
            <DisplayProduct />
          </div>
        </dataContext.Provider>
      </sizeContext.Provider>
    </>
  )
}
export default App
